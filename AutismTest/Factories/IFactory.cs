﻿using AutismTest.Domain;
using AutismTest.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutismTest
{
    public interface IFactory
    {
        ExerciseParentPageModel CreateExercisePageModel(ApplicationUser user, Test test);
        QuestionModel CreateQuestionModel(Question question);
        IList<UserModel> CreateUserModel(UserManager<ApplicationUser> userManager);
        IList<ExerciseModel> CreateTestSummary(int testId);
    }
}
