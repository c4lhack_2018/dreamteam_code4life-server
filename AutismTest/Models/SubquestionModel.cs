﻿namespace AutismTest.Models
{
    public class SubquestionModel
    {
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public string Body { get; set; }
        public int Order { get; set; }
    }
}