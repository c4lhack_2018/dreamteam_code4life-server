﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutismTest.Models
{
    public class QuestionModel
    {
        public int Id { get; set; }
        public string Body { get; set; }
        public int Threshold { get; set; }
        public int ThresholdPassQuestionId { get; set; }
        public QuestionModel ThresholdPassQuestion { get; set; }
        public int ThresholdFailQuestionId { get; set; }
        public QuestionModel ThresholdFailQuestion { get; set; }
        public int SecondaryThreshold { get; set; }
        public int SecondaryThresholdQuestionId { get; set; }
        public QuestionModel SecondaryThresholdQuestion { get; set; }
        public IList<SubquestionModel> Subquestions { get; set; }
    }
}
