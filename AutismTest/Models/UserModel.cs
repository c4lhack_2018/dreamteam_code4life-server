﻿using AutismTest.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutismTest.Models
{
    public class UserModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public int TestId { get; set; }
        public int Score { get; set; }
        public int ExerciseCount { get; set; }
    }
}
