﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutismTest.Models
{
    public class TagModel
    {
        public int Id { get; set; }
        public int ExerciseId { get; set; }
        public string Name { get; set; }
    }
}
