﻿using AutismTest.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutismTest.Domain
{
    public class TestExercise : BaseEntity
    {
        public int TestId { get; set; }
        public int ExerciseId { get; set; }
        public Result Result { get; set; }
    }
}
