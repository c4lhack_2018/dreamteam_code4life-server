﻿using AutismTest.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutismTest.Domain
{
    public class Test : BaseEntity
    {
        public string UserId { get; set; }
        public Result Result { get; set; }
    }
}
