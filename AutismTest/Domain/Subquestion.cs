﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutismTest.Domain
{
    public class Subquestion : BaseEntity
    {
        public int QuestionId { get; set; }
        public string Body { get; set; }
        public int Order { get; set; }
    }
}
