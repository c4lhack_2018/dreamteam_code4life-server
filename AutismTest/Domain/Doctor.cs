﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutismTest.Domain
{
    public class Doctor : BaseEntity
    {
        public string Name { get; set; }
        public string Address { get; set; }
    }
}
