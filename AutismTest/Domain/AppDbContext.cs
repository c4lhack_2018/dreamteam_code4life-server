﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using AutismTest.Configurations;

namespace AutismTest.Domain
{
    public class AppDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Test> Tests { get; set; }
        public DbSet<Exercise> Exercises { get; set; }
        public DbSet<TestExercise> TestExercises { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Subquestion> Subquestions { get; set; }
        public DbSet<MedicalInstitution> MedicalInstitution { get; set; }
        public DbSet<ObservationSheet> ObservationSheet { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Doctor> Doctors { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            IdentityConfiguration.Configure(builder);
        }
    }
}
