﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutismTest.Data;
using AutismTest.Domain;
using Microsoft.AspNetCore.Identity;

namespace AutismTest.Services
{
    public class Service : IService
    {
        private readonly IRepository<Test> _testRepository;
        private readonly IRepository<Subquestion> _subquestionRepository;
        private readonly IRepository<TestExercise> _testExerciseRepository;
        private readonly IRepository<Tag> _tagRepository;

        public Service(
            IRepository<Test> testRepository,
            IRepository<Subquestion> subquestionRepository,
            IRepository<TestExercise> testExerciseRepository,
            IRepository<Tag> tagRepository)
        {
            _testRepository = testRepository;
            _subquestionRepository = subquestionRepository;
            _testExerciseRepository = testExerciseRepository;
            _tagRepository = tagRepository;
        }

        public Test GetTestByUserId(string userId)
        {
            return _testRepository.GetAll().FirstOrDefault(t => t.UserId == userId);
        }

        public IList<Subquestion> GetSubquestionsByQuestionId(int id)
        {
            var questionSubquestion = _subquestionRepository.GetAll()
                .Where(subq => subq.QuestionId == id)
                .OrderBy(subq => subq.Order)
                .ToList();

            return questionSubquestion;
        }

        public TestExercise GetTestExercise(int testId, int exerciseId)
        {
            return _testExerciseRepository.GetAll().FirstOrDefault(te => te.TestId == testId && te.ExerciseId == exerciseId);
        }

        public IList<Tag> GetTagsByExerciseId(int exerciseId)
        {
            return _tagRepository.GetAll().Where(t => t.ExerciseId == exerciseId).ToList();
        }


        public IList<TestExercise> GetTestExercisesByTestId(int testId)
        {
            var query = _testExerciseRepository.GetAll()
                .Where(testExercise => testExercise.TestId == testId)
                .OrderBy(testExercise => testExercise.ExerciseId)
                .ToList();

            return query;
        }

        public IList<TestExercise> GetTestExerciseWithoutTestId(int testId)
        {
            var query = _testExerciseRepository.GetAll()
                .Where(testExercise => testExercise.TestId != testId)
                .ToList();

            return query;
        }
    }
}
