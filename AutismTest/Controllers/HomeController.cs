﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutismTest.Domain;
using AutismTest.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AutismTest.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return Content("");
        }
    }
}
