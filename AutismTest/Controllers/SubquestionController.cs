﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AutismTest.Data;
using AutismTest.Models;
using AutismTest.Domain;
using AutismTest.Services;

namespace AutismTest.Controllers
{
    [Produces("application/json")]
    [Route("api/Subquestion")]
    public class SubquestionsController : Controller
    {
        private readonly IRepository<Subquestion> _subquestionRepository;

        public SubquestionsController(IRepository<Subquestion> subquestionRepository)
        {
            this._subquestionRepository = subquestionRepository;
        }

        // GET: api/Subquestions
        [HttpGet]
        public IEnumerable<Subquestion> GetSubquestions()
        {
            return _subquestionRepository.GetAll();
        }

        // GET: api/Subquestions/5
        [HttpGet("{id}")]
        public IActionResult GetSubquestion([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var subquestion = _subquestionRepository.GetById(id);

            if (subquestion == null)
            {
                return NotFound();
            }

            return Ok(subquestion);
        }

        // PUT: api/Subquestions/5
        [HttpPut("{id}")]
        public IActionResult PutSubquestion([FromRoute] int id,[FromBody] Subquestion subquestion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != subquestion.Id)
            {
                return BadRequest();
            }

            try
            {
                _subquestionRepository.Update(subquestion);
            }


            catch (DbUpdateConcurrencyException)
            {
                if (!SubquestionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Subquestions
        [HttpPost]
        public IActionResult PostSubquestion([FromBody] Subquestion subquestion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _subquestionRepository.Insert(subquestion);

            return CreatedAtAction("GetSubquestion", new { id = subquestion.Id }, subquestion);
        }

        // DELETE: api/Subquestions/5
        [HttpDelete("{id}")]
        public IActionResult DeleteSubquestion([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var subquestion = _subquestionRepository.GetById(id);
            if (subquestion == null)
            {
                return NotFound();
            }

            _subquestionRepository.Delete(subquestion);

            return Ok(subquestion);
        }

        public bool SubquestionExists(int id)
        {
            return _subquestionRepository.Exists(id);
        }
    }
}