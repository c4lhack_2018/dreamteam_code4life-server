﻿using AutismTest.Core;
using AutismTest.Data;
using AutismTest.Domain;
using AutismTest.Models;
using AutismTest.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutismTest.Controllers
{

    [Route("api/Account")]
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger _logger;
        private readonly IRepository<Test> _testRepository;
        private readonly IService _service;
        private readonly IFactory _factory;

        public AccountController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ILogger<AccountController> logger,
            IRepository<Test> testRepository,
            IService service,
            IFactory factory)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _testRepository = testRepository;
            _service = service;
            _factory = factory;
        }


        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login([FromBody] LoginModel loginModel, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {

                var result = await _signInManager.PasswordSignInAsync(loginModel.Email, loginModel.Password, loginModel.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    var user = await _userManager.FindByEmailAsync(loginModel.Email);
                    if (user == null)
                    {
                        return BadRequest();
                    }

                    if (user.IsAdmin)
                    {
                        var model = _factory.CreateUserModel(_userManager);

                        return Ok(model);
                    }
                    else
                    {
                        var test = _service.GetTestByUserId(user.Id);
                        if (test == null)
                        {
                            return BadRequest();
                        }

                        var model = _factory.CreateExercisePageModel(user, test);

                        return Ok(model);
                    }
                }
            }

            return StatusCode(422);
        }

        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> Register([FromBody] RegisterModel registerModel)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = registerModel.Email, Email = registerModel.Email };
                var result = await _userManager.CreateAsync(user, registerModel.Password);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);

                    var test = _service.GetTestByUserId(user.Id);
                    if (test != null)
                    {
                        return BadRequest();
                    }
                    else
                    {
                        test = new Test()
                        {
                            UserId = user.Id,
                            Result = Result.None
                        };

                        _testRepository.Insert(test);
                    }

                    var model = _factory.CreateExercisePageModel(user, test);

                    return Ok(model);
                }
            }


            return StatusCode(422);
        }

        [HttpPost]
        [Route("Logout")]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();

            return Ok();
        }
    }
}
