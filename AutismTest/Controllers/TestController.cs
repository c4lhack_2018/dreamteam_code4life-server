﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AutismTest.Data;
using AutismTest.Models;
using AutismTest.Domain;

namespace AutismTest.Controllers
{
    [Produces("application/json")]
    [Route("api/Test")]
    public class TestsController : Controller
    {
        private readonly IRepository<Test> _testRepository;
        private readonly IFactory _factory;

        public TestsController(
            IRepository<Test> testRepository,
            IFactory factory)
        {
            _testRepository = testRepository;
            _factory = factory;
        }

        // GET: api/Tests
        [HttpGet]
        public IEnumerable<Test> GetTests()
        {
            return _testRepository.GetAll();
        }

        // GET: api/Tests/5
        [HttpGet("{id}")]
        public IActionResult GetTest([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var test = _testRepository.GetById(id);

            if (test == null)
            {
                return NotFound();
            }

            return Ok(test);
        }

        // PUT: api/Tests/5
        [HttpPut("{id}")]
        public IActionResult PutTest([FromRoute] int id,[FromBody] Test test)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != test.Id)
            {
                return BadRequest();
            }

            try
            {
                _testRepository.Update(test);
            }


            catch (DbUpdateConcurrencyException)
            {
                if (!TestExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Tests
        [HttpPost]
        public IActionResult PostTest([FromBody] Test test)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _testRepository.Insert(test);

            return CreatedAtAction("GetTest", new { id = test.Id }, test);
        }

        // DELETE: api/Tests/5
        [HttpDelete("{id}")]
        public IActionResult DeleteTest([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var test = _testRepository.GetById(id);
            if (test == null)
            {
                return NotFound();
            }

            _testRepository.Delete(test);

            return Ok(test);
        }

        // api/Test/TestSummary/5
        [HttpGet]
        [Route("TestSummary/{id}")]
        public IActionResult GetTestSummary(int id)
        {
            var model = _factory.CreateTestSummary(id);

            return Ok(model);
        }

        private bool TestExists(int id)
        {
            return _testRepository.Exists(id);
        }
    }
}